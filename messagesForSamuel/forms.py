from django import forms
from .models import *
from django.utils.translation import ugettext_lazy as _


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['senderName', 'message']
        labels = {
            'senderName': _('Your Name'),
        }
